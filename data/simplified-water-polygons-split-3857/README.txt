
This data was downloaded from osmdata.openstreetmap.de which offers
extracts and processings of OpenStreetMap data.

See https://osmdata.openstreetmap.de/ for details.


PACKAGE CONTENT
===============

This package contains OpenStreetMap data of the
coastline water polygons, simplified for rendering at low zooms and split into a grid.

Layers contained are:

simplified_water_polygons.shp:

  14408 Polygon features
  Mercator projection (EPSG: 3857)
  Extent: (-20037507, -14773406) - (20037508, 20037508)
  In geographic coordinates: (-180.000, -78.733) - (180.000, 85.051)

Date of the data used is 2020-10-19T00:00:00Z

You can find more information on this data set at

https://osmdata.openstreetmap.de/data/water-polygons


LICENSE
=======

This data is Copyright 2020 OpenStreetMap contributors. It is
available under the Open Database License (ODbL).

For more information see https://www.openstreetmap.org/copyright

